﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWPswing : MonoBehaviour {	
	
	// Use this for initialization
	void Start () {
		StartCoroutine(ExecuteAfterTime(0.5f));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	IEnumerator ExecuteAfterTime(float time)
	{
		yield return new WaitForSeconds(time);		
	    Destroy(gameObject);
	}
}
