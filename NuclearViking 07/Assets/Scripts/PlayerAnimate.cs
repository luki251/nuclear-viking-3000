﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimate : MonoBehaviour {
	float Horizontal;
	float Vertical;
	
	Animator anim;
	int AnimDirection;
	SpriteRenderer spriteRend;

	void Start () {		
		anim = GetComponent<Animator>();
		//rb = GetComponent<Rigidbody2D>();
		spriteRend = GetComponent<SpriteRenderer>();
	}
	
	void Update () {
		Horizontal = Input.GetAxisRaw("Horizontal");
		Vertical = Input.GetAxisRaw("Vertical");
		
		AnimDirection = anim.GetInteger("Direction");
		
		//idle
		if(Horizontal == 0 && Vertical == 0)
			anim.SetBool("Walking",false);
		else
			anim.SetBool("Walking",true);
		
		//directions
		if(Horizontal < 0 && Vertical < 0)
			anim.SetInteger("Direction",1);
		else if(Horizontal < 0 && Vertical > 0)
			anim.SetInteger("Direction",2);
		else if(Horizontal > 0 && Vertical < 0)
			anim.SetInteger("Direction",0);
		else if(Horizontal > 0 && Vertical > 0)
			anim.SetInteger("Direction",3);
		else if(Horizontal < 0 && Vertical == 0){
			if(AnimDirection == 3)
				anim.SetInteger("Direction",2);
			else if(AnimDirection == 0)
				anim.SetInteger("Direction",1);
		}
		else if(Horizontal > 0 && Vertical == 0){
			if(AnimDirection == 2)
				anim.SetInteger("Direction",3);
			else if(AnimDirection == 1)
				anim.SetInteger("Direction",0);
		}
		else if(Vertical < 0 && Horizontal == 0){
			if(AnimDirection == 2)
				anim.SetInteger("Direction",1);
			else if(AnimDirection == 3)
				anim.SetInteger("Direction",0);
		}
		else if(Vertical > 0 && Horizontal == 0){
			if(AnimDirection == 1)
				anim.SetInteger("Direction",2);
			else if(AnimDirection == 0)
				anim.SetInteger("Direction",3);
		}
		
		if(AnimDirection == 0 || AnimDirection == 2)
			spriteRend.flipX = true;
		else
			spriteRend.flipX = false;
		
		if(Input.GetAxisRaw("Horizontal") == 0f &&  Input.GetAxisRaw("Vertical") == 0f){
			anim.SetBool("Walking",false);
		}
		else{
			anim.SetBool("Walking",true);
		}


		
	}


}
