﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponDamage : MonoBehaviour {
	int randomNumber;
	public float Time;

	// Use this for initialization
	void Start () {


		//yield return new WaitForSeconds(5);
		//print("5sec");
		StartCoroutine(ExecuteAfterTime(Time));
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	void OnTriggerEnter2D(Collider2D col){
		//print(col.gameObject.tag);
		if(col.gameObject.tag == "Enemy")
			Destroy(col.gameObject);

		//PlayerAttack.cooldown.fillAmount += 0.05f;

		//PlayerMove.scoreInt += 10;


		//random number generator for rare drops
		randomNumber = Random.Range (0, 5);

		Debug.Log ("the random number is: " + randomNumber.ToString ());

		if (randomNumber == 1) {
			Debug.Log ("rare drop");
		}

	}
	
	IEnumerator ExecuteAfterTime(float time)
	{
		yield return new WaitForSeconds(time);		
	    Destroy(gameObject);
	}


}
