﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destroyboom : MonoBehaviour {

	void Start () {
		StartCoroutine(ExecuteAfterTime(1f));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	IEnumerator ExecuteAfterTime(float time)
	{
		yield return new WaitForSeconds(time);		
	    Destroy(gameObject);
	}
}
