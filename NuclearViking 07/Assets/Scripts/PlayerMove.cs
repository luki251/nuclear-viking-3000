﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMove : MonoBehaviour {
		public float MaxMovementSpeed;
		float MovementSpeed;
		//float HorizontalMovementSpeed;
		//float VerticalMovementSpeed;
		public float Acceleration;
		float Horizontal;
		float Vertical;
		private Rigidbody2D rb;
		//Vector3 Direction;
		Animator anim;
		int AnimDirection;
		
		/*public float moveTime = 0.1f;           //Time it will take object to move, in seconds.
        public LayerMask blockingLayer;         //Layer on which collision will be checked.

		private BoxCollider2D boxCollider;      //The BoxCollider2D component attached to this object.
        private Rigidbody2D rb2D;               //The Rigidbody2D component attached to this object.
        private float inverseMoveTime;   */       //Used to make movement more efficient.

		//health variable
		public static int healthInt;

		public static int scoreInt;
		private Text textDisplay;
	public static int waveCounter;
	
	void Start () {			
		scoreInt = 0;
		textDisplay = GameObject.Find ("scoreText").GetComponent<Text>();

		waveCounter = 0;

			MovementSpeed = 0.1f;
			anim = GetComponent<Animator>();
			rb = GetComponent<Rigidbody2D>();
			//rigidbody.freezeRotation = true;
			//rigidbody.freezeRotation
			//Sprite LD = Resources.Load("LD", typeof(Sprite)) as Sprite;
			//SpriteRenderer.Sprite=new Sprite(LD);	

			healthInt = 3;
		AIController.isDead = false;



			GameObject.Find ("overlayScreen").GetComponent<Image> ().enabled = false;
			GameObject.Find ("overlayScreenText").GetComponent<Text> ().enabled = false;
	}

	void FixedUpdate() {
		textDisplay.text = "<b>Score: </b>" + scoreInt.ToString ();

		//waves script vvv
		if (waveCounter == 0) {
			if (PlayerMove.scoreInt >= 300) {
				GameObject.Find ("WaveAudio2").GetComponent<EasyFadeIn> ().enabled = true;

				GameObject.Find ("overlayScreenText").GetComponent<Text> ().text = "WAVE 2";

				GameObject.Find ("mainSpawner").GetComponent<bb> ().spawnTime = 2.8f;

				GameObject.Find ("mainSpawner").GetComponent<bb> ().enabled = false;


				waveCounter += 1;
				Debug.Log("waveCounter: " + waveCounter.ToString());

				StartCoroutine (OverlayScreen ());

			}
		}

		if (waveCounter == 1) {
			if (PlayerMove.scoreInt >= 1000) {
				GameObject.Find ("WaveAudio2").GetComponent<EasyFadeOut> ().enabled = true;
				GameObject.Find ("WaveAudio3").GetComponent<EasyFadeIn> ().enabled = true;

				GameObject.Find ("overlayScreenText").GetComponent<Text> ().text = "WAVE 3";

				GameObject.Find ("mainSpawner").GetComponent<bb> ().spawnTime = 1.9f;
				GameObject.Find ("mainSpawner").GetComponent<bb> ().enabled = false;

				waveCounter += 1;
				Debug.Log("waveCounter: " + waveCounter.ToString());

				StartCoroutine (OverlayScreen ());

			}
		}

		if (waveCounter == 2) {
			if (PlayerMove.scoreInt >= 2500) {
				GameObject.Find ("WaveAudio3").GetComponent<EasyFadeOut> ().enabled = true;
				GameObject.Find ("WaveAudio4").GetComponent<EasyFadeIn> ().enabled = true;

				GameObject.Find ("overlayScreenText").GetComponent<Text> ().text = "WAVE 4";

				GameObject.Find ("mainSpawner").GetComponent<bb> ().spawnTime = 1f;
				GameObject.Find ("mainSpawner").GetComponent<bb> ().enabled = false;

				waveCounter += 1;
				Debug.Log("waveCounter: " + waveCounter.ToString());

				StartCoroutine (OverlayScreen ());

			}
		}

		if (waveCounter == 3) {
			if (PlayerMove.scoreInt >= 5000) {
				GameObject.Find ("WaveAudio4").GetComponent<EasyFadeOut> ().enabled = true;
				GameObject.Find ("WaveAudio5").GetComponent<EasyFadeIn> ().enabled = true;

				
				GameObject.Find ("overlayScreenText").GetComponent<Text> ().text = "ENDLESS MODE";

				GameObject.Find ("mainSpawner").GetComponent<bb> ().spawnTime = 0.7f;
				GameObject.Find ("mainSpawner").GetComponent<bb> ().enabled = false;

				waveCounter += 1;
				Debug.Log("waveCounter: " + waveCounter.ToString());

				StartCoroutine (OverlayScreen ());

			}
		}

		if (waveCounter == 4) {
			if (PlayerMove.scoreInt >= 10000) {
				GameObject.Find ("overlayScreenText").GetComponent<Text> ().text = "BONUS";

				healthInt += 1;

				waveCounter += 1;
				Debug.Log("waveCounter: " + waveCounter.ToString());

				StartCoroutine (OverlayScreen ());

			}
		}

	
		if (healthInt == 4) {
			GameObject.Find ("heart4").GetComponent<Image> ().enabled = true;
			GameObject.Find ("heart3").GetComponent<Image> ().enabled = true;
			GameObject.Find ("heart2").GetComponent<Image> ().enabled = true;
			GameObject.Find ("heart1").GetComponent<Image> ().enabled = true;


		}

		if (healthInt == 3) {
			GameObject.Find ("heart4").GetComponent<Image> ().enabled = false;
			GameObject.Find ("heart3").GetComponent<Image> ().enabled = true;
			GameObject.Find ("heart2").GetComponent<Image> ().enabled = true;
			GameObject.Find ("heart1").GetComponent<Image> ().enabled = true;


		}


		if (healthInt == 2) {
			GameObject.Find ("heart4").GetComponent<Image> ().enabled = false;

			GameObject.Find ("heart3").GetComponent<Image> ().enabled = false;
			GameObject.Find ("heart2").GetComponent<Image> ().enabled = true;
			GameObject.Find ("heart1").GetComponent<Image> ().enabled = true;
		}

		if (healthInt == 1) {
			GameObject.Find ("heart4").GetComponent<Image> ().enabled = false;
			GameObject.Find ("heart3").GetComponent<Image> ().enabled = false;
			GameObject.Find ("heart2").GetComponent<Image> ().enabled = false;
			GameObject.Find ("heart1").GetComponent<Image> ().enabled = true;
		}

		if (healthInt < 1) {
			AIController.isDead = true;

			Debug.Log (healthInt.ToString());

			GameObject.Find ("heart4").GetComponent<Image> ().enabled = false;
			GameObject.Find ("heart3").GetComponent<Image> ().enabled = false;
			GameObject.Find ("heart2").GetComponent<Image> ().enabled = false;
			GameObject.Find ("heart1").GetComponent<Image> ().enabled = false;

			GameObject.Find ("overlayScreen").GetComponent<Image> ().enabled = true;
			GameObject.Find ("overlayScreenText").GetComponent<Text> ().text = "YOU DIED";
			GameObject.Find ("overlayScreenText").GetComponent<Text> ().enabled = true;
		

			Destroy (gameObject);

		}
	}

	void fadeIn() {
		
	}
	IEnumerator OverlayScreen() {

		yield return new WaitForSeconds (1);

		GameObject.Find ("overlayScreen").GetComponent<Image> ().enabled = true;
		GameObject.Find ("overlayScreenText").GetComponent<Text> ().enabled = true;

		yield return new WaitForSeconds (3);

		GameObject.Find ("overlayScreen").GetComponent<Image> ().enabled = false;
		GameObject.Find ("overlayScreenText").GetComponent<Text> ().enabled = false;

		GameObject.Find ("mainSpawner").GetComponent<bb> ().enabled = true;

	}
		
	void Update () {	

		/*if(Input.GetAxisRaw("Horizontal") > 0.5f ||  Input.GetAxisRaw("Horizontal") < -0.5f){
			Horizontal = Input.GetAxisRaw("Horizontal") * MovementSpeed * Time.deltaTime;
			if(Input.GetAxisRaw("Vertical")!=0)
				Horizontal*=0.7f;				
		}
		else{
			Horizontal = 0;
		}
		if(Input.GetAxisRaw("Vertical") > 0.5f ||  Input.GetAxisRaw("Vertical") < -0.5f){
			Vertical = Input.GetAxisRaw("Vertical") * MovementSpeed * Time.deltaTime;
			if(Input.GetAxisRaw("Horizontal")!=0)
				Vertical*=0.7f;
		}
		else{
			Vertical = 0;
		}
		transform.Translate(Horizontal,Vertical,0f);*/
		rb.velocity = Vector3.zero;
		
		if(Input.GetAxisRaw("Horizontal") > 0.5f ||  Input.GetAxisRaw("Horizontal") < -0.5f){
			Horizontal = Input.GetAxisRaw("Horizontal") * MovementSpeed * Time.deltaTime;
			if(MovementSpeed < MaxMovementSpeed)
				MovementSpeed += Acceleration;
			if(Input.GetAxisRaw("Vertical")!=0)
				Horizontal*=0.7f;
		}
		else{
			Horizontal = 0f;// Horizontal/Acceleration;
		}
		
		if(Input.GetAxisRaw("Vertical") > 0.5f ||  Input.GetAxisRaw("Vertical") < -0.5f){
			Vertical = Input.GetAxisRaw("Vertical") * MovementSpeed * Time.deltaTime;			
			if(MovementSpeed < MaxMovementSpeed)
				MovementSpeed += Acceleration;
			if(Input.GetAxisRaw("Horizontal")!=0)
				Vertical*=0.7f;
		}
		else{
			Vertical = 0f;// Vertical/Acceleration;
		}			
		
		transform.Translate(Horizontal,Vertical,0f);
		
		if(Input.GetAxisRaw("Horizontal") == 0f &&  Input.GetAxisRaw("Vertical") == 0f){
			MovementSpeed = 0.1f;
		}
		else if(MovementSpeed < MaxMovementSpeed){
			MovementSpeed += Acceleration;
		}

		//anim
		/*AnimDirection = anim.GetInteger("Direction");
		
		if(Horizontal < 0 && Vertical < 0)
			anim.SetInteger("Direction",0);
		else if(Horizontal < 0 && Vertical > 0)
			anim.SetInteger("Direction",1);
		else if(Horizontal > 0 && Vertical < 0)
			anim.SetInteger("Direction",2);
		else if(Horizontal > 0 && Vertical > 0)
			anim.SetInteger("Direction",3);
		else if(Horizontal < 0 && Vertical == 0){
			if(AnimDirection == 3)
				anim.SetInteger("Direction",1);
			else if(AnimDirection == 2)
				anim.SetInteger("Direction",0);
		}
		else if(Horizontal > 0 && Vertical == 0){
			if(AnimDirection == 1)
				anim.SetInteger("Direction",3);
			else if(AnimDirection == 0)
				anim.SetInteger("Direction",2);
		}
		else if(Vertical < 0 && Horizontal == 0){
			if(AnimDirection == 1)
				anim.SetInteger("Direction",0);
			else if(AnimDirection == 3)
				anim.SetInteger("Direction",2);
		}
		else if(Vertical > 0 && Horizontal == 0){
			if(AnimDirection == 0)
				anim.SetInteger("Direction",1);
			else if(AnimDirection == 2)
				anim.SetInteger("Direction",3);
		}*/
	}
}