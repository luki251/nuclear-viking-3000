﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShockwaveDamage : MonoBehaviour {

	void Start () {


		//yield return new WaitForSeconds(5);
		//print("5sec");
		StartCoroutine(ExecuteAfterTime(1));
	}

	// Update is called once per frame
	void Update () {

	}

	void OnTriggerEnter2D(Collider2D col){
		//print(col.gameObject.tag);
		if(col.gameObject.tag == "Enemy")
			Destroy(col.gameObject);

		PlayerMove.scoreInt += 100;

	
	}

	IEnumerator ExecuteAfterTime(float time)
	{
		yield return new WaitForSeconds(time);		
		Destroy(gameObject);
	}

}
