﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAttack : MonoBehaviour {

	Animator anim;
	int AnimDirection;
	
	public GameObject Weapon;
	public GameObject Polygon;
	public GameObject Swing;
	public GameObject SwingF;
	public GameObject SwingF2;
	public GameObject SwingF3;
	public GameObject Boom;

	public static Image cooldown;
	// Use this for initialization
	void Start () {	
		anim = GetComponent<Animator>();
		cooldown = GameObject.Find ("power_bar").GetComponent<Image> ();

	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetButtonDown("Fire1")){
			var weaponClone = Instantiate (Weapon, transform.position, transform.rotation);
			weaponClone.transform.parent = transform;
			var weaponClone2 = Instantiate (Weapon, transform.position, transform.rotation);
			weaponClone2.transform.parent = transform;
			var weaponClone3 = Instantiate (Weapon, transform.position, transform.rotation);
			weaponClone3.transform.parent = transform;		
			
			
			AnimDirection = anim.GetInteger("Direction");
			if(AnimDirection == 1){
				weaponClone.transform.Translate(-1f,-1f,0f);
				weaponClone2.transform.Translate(-1f,0f,0f);
				weaponClone3.transform.Translate(0.2f,-1.2f,0f);
				var swingClone = Instantiate (Swing, transform.position, transform.rotation);
				swingClone.transform.parent = transform;
				swingClone.transform.Translate(-0.65f,-0.35f,0f);				
			}
			else if(AnimDirection == 2){
				weaponClone.transform.Translate(-1f,1f,0f);
				weaponClone2.transform.Translate(-1f,0f,0f);
				weaponClone3.transform.Translate(0.2f,1.2f,0f);	
					
				var swingClone = Instantiate (SwingF2, transform.position, transform.rotation);
				swingClone.transform.parent = transform;
				swingClone.transform.Translate(-0.65f,0f,0f);
				
			}
			else if(AnimDirection == 0){
				weaponClone.transform.Translate(1f,-1f,0f);
				weaponClone2.transform.Translate(1f,0f,0f);
				weaponClone3.transform.Translate(0.2f,-1.2f,0f);
				var swingClone = Instantiate (SwingF, transform.position, transform.rotation);
				swingClone.transform.parent = transform;
				swingClone.transform.Translate(0.65f,-0.35f,0f);				
			}
			else if(AnimDirection == 3){
				weaponClone.transform.Translate(1f,1f,0f);
				weaponClone2.transform.Translate(1f,0f,0f);
				weaponClone3.transform.Translate(0.2f,1.2f,0f);
				
				var swingClone = Instantiate (SwingF3, transform.position, transform.rotation);
				swingClone.transform.parent = transform;
				swingClone.transform.Translate(0.65f,0f,0f);
			}
		}

		if (PlayerMove.waveCounter >= 0) {
			if (cooldown.fillAmount == 1) {
				if (Input.GetButtonDown ("Fire2")) {
					//Debug.Log ("test");
					var shockwaveClone = Instantiate (Polygon, transform.position, transform.rotation);
					var boom = Instantiate (Boom, transform.position, transform.rotation);
					boom.transform.Translate(-0.1428572f,3f,0f);
					//shockwaveClone.transform.parent = transform;
					//shockwaveClone.transform.parent = null;

					cooldown.fillAmount = 0.0f;
					//weaponClone.gameObject.DestroyAfterTime(2);
			}

			}	
		}
	}
}
