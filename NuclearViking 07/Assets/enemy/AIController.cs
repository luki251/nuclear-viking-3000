using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AIController : MonoBehaviour
{
    public Transform target;
	private Rigidbody2D rb;
	private Vector3 targetDirection;

	public static bool isDead;
	bool wait = true;

	Animator anim;
	int AnimDirection;
	SpriteRenderer spriteRend;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
		
		anim = GetComponent<Animator>();
		spriteRend = GetComponent<SpriteRenderer>();
		
		//rb.angularVelocity = 0f;
		StartCoroutine(ExecuteAfterTime(1));	
    }

    void Update()
    {
		if (!isDead && !wait) {
			rb.velocity = Vector3.zero;
			//rb.angularVelocity = Vector3.zero;
			targetDirection = (target.position - transform.position);
			
			rb.AddForce (targetDirection * (1 / targetDirection.magnitude) * 100f/** 0.08f*/);
						
			//print(Vector3.Angle(transform.position, target.position));
			/*if(Vector3.Angle(transform.position, target.position)>60){
				anim.SetBool("Front",false);				
				if(targetDirection.x > 0)
					spriteRend.flipX = false;
				else
					spriteRend.flipX = true;
			}
			else{
				anim.SetBool("Front",true);
				if(targetDirection.x > 0)
					spriteRend.flipX = true;
				else
					spriteRend.flipX = false;
			}*/
			
		}

		if (isDead) {
			Destroy (gameObject);
			//PlayerAttack.cooldown.fillAmount += 0.05f;
			//PlayerMove.scoreInt += 10;
		}
    }

	void OnCollisionEnter2D (Collision2D col)
	{
		if (col.gameObject.tag == "Player") {
			PlayerMove.healthInt -= 1;
			Debug.Log ("Touched player");	
		}

		if (col.gameObject.tag == "shield") {
			Destroy (col.collider);
			Debug.Log ("Touched shield");
		}
	}
	
	IEnumerator ExecuteAfterTime(float time)
	{
		yield return new WaitForSeconds(time);		
	    wait = false;
	}
	
	 void OnDestroy(){
		PlayerAttack.cooldown.fillAmount += 0.05f;
		PlayerMove.scoreInt += 10;
	 }
}
